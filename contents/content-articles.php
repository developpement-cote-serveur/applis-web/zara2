<?php
    $search = (isset($_GET['search']) ? $_GET['search'] : null);
    $sort = (isset($_GET['sort']) ? $_GET['sort'] : null);
?>

<section id="articles">
    <div class="search">
        <h2>Rechercher un article</h2>
        <form class="f_search" action="articles.php" method="get">
            <input 
                type="text"
                name="search"
                placeholder="Entrez un mot-clé"
                value="<?php echo ($search ? $search : ''); ?>">
            <input type="submit" value="Rechercher" class="button icon fa-search">
        </form>
    </div>

    <div class="sort">
        Trier par intitulé d'article :
        <form action="articles.php" method="get">
            <input name="sort" type="submit" value="ASC" class="<?php echo ($sort == 'ASC' ? 'active' : ''); ?>">
            <input name="sort" type="submit" value="DESC" class="<?php echo ($sort == 'DESC' ? 'active' : ''); ?>">
        </form>
    </div>
<?php

    $articles = getArticles($mysqli, null, $search, $sort);

    if (count($articles) < 1) {
        echo "Désolé ! Aucun article n'a été trouvé...";
    } else {

?>

    <div class="box alt">
        <div class="teasers-wrapper row 50% uniform">

        <?php
            // Itération sur les articles
            foreach ($articles as $article) {
        ?>
                <article class="teaser 4u">
                    <a href="article.php?id=<?php echo $article['id']; ?>"
                       title="Accéder à l'article">
                        <header>
                            <h2><?php echo $article['intitule']; ?></h2>
                            <span class="image fit">
                                <img src="uploads/images/articles/<?php echo $article['photo']; ?>" alt="" />
                            </span>
                        </header>

                        <div class="content">
                            <?php echo $article['description']; ?>
                        </div>

                        <footer>
                            <span class="prix">
                                <?php echo $article['prix']; ?> €
                            </span>
                            <span class="tag">
                                <?php echo $article['c_intitule']; ?>
                            </span>
                        </footer>
                    </a>
                </article>
            <?php

        }
        ?>

        </div>
    </div>
<?php
    } 
?>

</section>
