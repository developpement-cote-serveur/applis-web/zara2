<?php
$idArticle = (int)$_GET['id'];
$article = getArticleById($mysqli, $idArticle);

if ($article === null) {
    echo "Aucun article n'a été trouvé, veuillez revenir à la liste des articles.";
} else {
    ?>

    <article class="article image fit">
        <header>
            <h1><?php echo $article['intitule']; ?></h1>

            <div class="photos">
                <?php
                    $photos = getPhotosByArticleId($mysqli, $idArticle);

                    if (!is_null($photos) && count($photos) < 2) {
                ?>
                        <img src="uploads/images/articles/<?php echo $photos[0]['uri']; ?>" alt="" />
                <?php
                    } elseif (!is_null($photos)) {
                ?>
                    <div id="carousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#carousel" data-slide-to="0" class="active"></li>
                            <?php
                                for ($i = 1; $i < count($photos); $i++) {
                            ?>
                                <li data-target="#carousel" data-slide-to="<?php echo $i; ?>"></li>
                            <?php
                                }
                            ?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <?php
                                $active = 'active';
                                foreach ($photos as $photo) {
                            ?>
                                    <div class="item <?php echo $active;?>">
                                        <img src="uploads/images/articles/<?php echo $photo['uri']; ?>" alt="">
                                    </div>
                            <?php
                                    $active = '';
                                }
                            ?>
                        </div>
                    </div>
                <?php
                    }
                ?>
            </div>
        </header>

        <div class="content">
            <div class="row">
                <div class="col-xs-6">
                    Prix : <?php echo $article['prix']; ?> €
                </div>

                <div class="col-xs-6 text-right">
                    <span class="tag">
                        <?php echo $article['categorie']; ?>
                    </span>
                </div>
            </div>

            <div class="tailles">
                Tailles :

                <div>
                    <?php
                        $tailles = getTaillesByArticleId($mysqli, $idArticle);

                        foreach ($tailles as $taille) {
                            echo '<span class="taille">' . strtoupper($taille['intitule']) . '</span>';
                        }
                    ?>
                </div>
            </div>

            <div class="couleurs">
                Couleurs :

                <div>
                    <?php
                        $couleurs = getCouleursByArticleId($mysqli, $idArticle);

                        foreach ($couleurs as $couleur) {
                            $hexa = getCouleur($couleur['intitule']);
                            echo '<span class="couleur" title="' . ucfirst($couleur['intitule']) . '"
                                  style="background-color:' . $hexa . '"></span>';
                        }
                    ?>
                </div>
            </div>

            <div class="description">
                <?php echo $article['description']; ?>
            </div>
        </div>

        <footer>
        </footer>
    </article>

<?php

}
?>
