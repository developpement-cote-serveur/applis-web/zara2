<h1>La nouvelle collection est arrivée !</h1>

<section id="articles">
    <div class="alt">
        <div class="teasers-wrapper row 50% uniform">
            <?php
                $articles = getArticles($mysqli, 10);

                foreach($articles as $article) {
            ?>
                    <article class="teaser 4u">
                        <a href="article.php?id=<?php echo $article['id']; ?>"
                            title="Accéder à l'article">
                            <header>
                                <h2><?php echo $article['intitule']; ?></h2>
                                <img src="uploads/images/articles/<?php echo $article['photo']; ?>" alt="" />
                            </header>

                            <div class="content">
                                <?php echo $article['description']; ?>
                            </div>

                            <footer>
                                <span class="prix">
                                    <?php echo $article['prix']; ?> €
                                </span>
                                <span class="tag">
                                    <?php echo $article['c_intitule']; ?>
                                </span>
                            </footer>
                        </a>
                    </article>
            <?php
                }
            ?>
        </div>
    </div>
</section>
